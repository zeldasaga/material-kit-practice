/*eslint-disable*/
import * as React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@mui/material/List";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";

// Images
// import Image from "@mui/material/Image";
import skyHigh from "assets/img/equipment-pics/sky-high.png";
import beefyBaby from "assets/img/equipment-pics/beefy-baby.png";
import SolidRag6x6 from "assets/img/equipment-pics/6x6-solid-rag.png";
import silk12x12 from "assets/img/equipment-pics/12x12-silk.jpg";
import by4WhiteSilver from "assets/img/equipment-pics/4x4-white-silver.png";
import by4SingleNet from "assets/img/equipment-pics/4x4.jpg";
import diffusion4x4 from "assets/img/equipment-pics/4x4-diffusion.jpg";
import floppy4x4 from "assets/img/equipment-pics/4x4-floppy.jpg";
import frame6x6 from "assets/img/equipment-pics/6x6-frame.jpg";
import largeClamp from "assets/img/equipment-pics/large-clamp.jpg";
import frame12x12 from "assets/img/equipment-pics/12x12-frame.jpg";
import danaDolly from "assets/img/equipment-pics/dana-dolly.jpg";
import sandBag from "assets/img/equipment-pics/sand-bag.jpg";
import roadFlagkit from "assets/img/equipment-pics/2x4-road-flag-kit.jpg";
//import fullSizeCStands from "assets/img/equipment-pics/";
//import miniCStands from "assets/img/equipment-pics/";
//import fullSizeComboStands from "assets/img/equipment-pics/";
//import smallComboStands from "assets/img/equipment-pics/";
import imageUnavailable from "assets/img/equipment-pics/image-unavailable.png";

// core components

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";

import Collapse from "@mui/material/Collapse";
import { ListItemText } from "@material-ui/core";
import { Grid, ListItemButton } from "@mui/material";

const useStyles = makeStyles(styles);
const textStyle = { color: "#3C4858" };
const imgHeight = "100px";
const IMAGES = {
  "Sky High": skyHigh,
  "Beefy Baby": beefyBaby,
  "6x6 Solid Rag": SolidRag6x6,
  "12x12 Silk": silk12x12,
  "4 By With White/Silver": by4WhiteSilver,
  "4x4 250/Hemp Diffusion": diffusion4x4,
  "4x4 Hemp Diffusion": "",
  "4x4 Single Net and Double Net": by4SingleNet,
  "2 4x4 Floppies": floppy4x4,
  "6x6 Frame with White, Silver, or Silk Option": frame6x6,
  "2x4 Road Flag Kit": roadFlagkit,
  "2 Large Clamps": largeClamp,
  "Assorted Grip Clips/Clamps": "",
  "12x12 Frame": frame12x12,
  "Dana Dolly with 5' and 10' Track": danaDolly,
  "7 Sand Bags": sandBag,
  "5 Full Size C Stands": imageUnavailable,
  "3 Mini C Stands": imageUnavailable,
  "3 Full Size Combo Stands": imageUnavailable,
  "2 Small Combo Stands": imageUnavailable,
};

export default function TrailerContents(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleEquipmentClick = () => {
    setOpen(!open);
  };
  const EQUIPMENTLIST = ({ items }) =>
    Object.entries(items).map(([k, v]) => (
      <Grid item xs={6} sm={6} md={4} lg={2} textAlign="center">
        <ListItemText
          primary={k}
          style={textStyle}
          className={classes.description}
        />
        <img src={v} height={imgHeight} />
      </Grid>
    ));
  return (
    <List>
      <ListItemButton onClick={handleEquipmentClick}>
        <ListItemText
          primary="Equipment"
          style={{ ...textStyle, borderBottom: 1, borderColor: "primary" }}
          className={classes.description}
        />
        {open ? (
          <ExpandLess style={textStyle} />
        ) : (
          <ExpandMore style={textStyle} />
        )}
      </ListItemButton>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Grid container spacing={2} justifyContent="center" alignItems="center">
          <EQUIPMENTLIST items={IMAGES} />
        </Grid>
      </Collapse>
    </List>
  );
}
